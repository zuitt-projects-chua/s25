db.fruits.aggregate([
		{
			$match: {"supplier": "Red Farms Inc."}
		},
		{
			$count: "totalItemsonSale"
		}
	])

db.fruits.aggregate([
		{
			$match: {"price": {$gt: 50}}
		},
		{
			$count: "noOfItemsPriceGreater50"
		}
	])

db.fruits.aggregate([
	{
		$match: {"onsale": true}
	},
	{
		$group: {_id: "$supplier",  avgPricePerSupplier: {$avg: "$price"}}
	}
	])

db.fruits.aggregate([
	{
		$match: {"onsale": true}
	},
	{
		$group: {_id: "$supplier",  maxPricePerSupplier: {$max: "$price"}}
	}
	])

db.fruits.aggregate([
	{
		$match: {"onsale": true}
	},
	{
		$group: {_id: "$supplier",  minPricePerSupplier: {$min: "$price"}}
	}
	])