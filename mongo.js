/*
Aggregation in Mongo DB
*/

db.fruits.insertMany([
		{
			"name": "Apple",
			"supplier": "Red Farms Inc.",
			"stocks": 20,
			"price": 40,
			"onsale": true
		},
		{
			"name": "Banana",
			"supplier": "Yellow Farms",
			"stocks": 15,
			"price": 20,
			"onsale": true,
		},
		{
			"name": "Kiwi",
			"supplier": "Green farming and Canning",
			"stocks": 25,
			"price": 50,
			"onsale": true,
		},
		{
			"name": "Mango",
			"supplier": "Yellow Farms",
			"stocks": 10,
			"price": 60,
			"onsale": true,
		},
		{
			"name": "Dragon Fruit",
			"supplier": "red Farms Inc.",
			"stocks": 10,
			"price": 60,
			"onsale": true,
		}
	])

db.fruits.aggregate([
		{
			$match: {"onSale": true}
		},
		{
			$group: {_id : "$supplier", totalstocks: {$sum: "$stocks"}}
		}
	])

/*
Aggregation in MongoDb

Process of gnerating manipulated data and perform operations to create filtered results that helps data analysis
Helps creating reports from analyzing the data provided in our documents
helps in arriving at summarized information not previously shown in our documents

Aggregation is done in 2 to 3 steps each stage called pipeline

1 $match used to pass the documents or get the documents that will pass the given condition
		{$match: {field: value}}

2 $group grouped elements/documents together an create an analysis

$group: {_id:"$<id>", fieldResult:"$<valueResult"}
the id field name refer to the available field name in the document to be aggregated
the totalstocks is the variable tobe set by the dveeloper

db.fruits.aggregate([
		{
			$match: {"supplier": "Red Farms Inc."}
		},
		{
			$group: {_id: "$notForSale", totalStocks: {$sum: "$stocks"}}
		}
	])
the _id to be grouped may also be changed sinced the matched fields will refer to a single supplier
*/

db.fruits.aggregate([
		{
			$match: {"onsale": true}
		},
		{
			$group: {_id: "$supplier", totalPrice: {$sum: "$price"}}
		}
	])

db.fruits.aggregate([
		{
			$match: {"onsale": true}
		},
		{
			$group: {_id: "$supplier", totalPrice: {$multiply: ["$price", "$stocks"]}}
		}
	])

db.fruits.aggregate([
		{
			$match: {"onsale": true}
		},
		{
			$group: {_id: "avgPriceOnSale", avgPrice: {$ave: "$price"}}
		}
	])
//displays supplier and inventory
db.fruits.aggregate([
		{
			$match: {"onsale": true}
		},
		{
			$project: {supplier: 1,_id: 0, inventory: {$multiply: ["$price", "$stocks"]}}
		}
	])
//displays supllier with details
db.fruits.aggregate([
		{
			$match: {"onsale": true}
		},
		{
			$project: {supplier: 1,_id: 0,price: 1, stocks: 1, inventory: {$multiply: ["$price", "$stocks"]}}
		}
	])
//the 1st stage works on first part of db syntax, second stage works on the second part of field projection part of main db syntax
db.fruits.aggregate([
	{
		$match: {"onsale": true}
	},
	{
		$group: {_id: "maxStocksonSale",  maxStock: {$max: "$stocks"}}
	}
])

//$min gets the lowest value of the values of the target field

db.fruits.aggregate([
		{
		$match: {"onsale": true}
	},
	{
		$group: {_id: "minStocksonSale",  minStock: {$min: "$stocks"}}
	}
	])

db.fruits.aggregate([
		{
		$match: {"onsale": true}
	},
	{
		$group: {_id: "$supplier",  minStock: {$min: "$stocks"}}
	}
	])

/*
Other stages
count is a stage we can add after a match stage to count all items
*/

db.fruits.aggregate([
		{
			$match: {"onsale": true}
		},
		{
			$count: "itemsOnSale"
		}
	])
db.fruits.aggregate([
		{
			$match: {"name": "Mango"}
		},
		{
			$count: "itemsOnSale"
		}
	])

db.fruits.aggregate([
		{
			$match: {"supplier": "Red Farms Inc."}
		},
		{
			$count: "itemsOnSale"
		}
	])

//counts the documents matched the conditon

db.fruits.aggregate([
		{
			$match: {"price": {$lt: 50}}
		},
		{
			$count: "priceLessThan50"
		}
	])

//find used to find and manipulate data while aggregate used to retrieve read only


//works how to retrieve the no of items a supplier is on sale
db.fruits.aggregate([
		{
		$match: {"onsale": true}
	},
	{
		$group: {_id: "$supplier",  noOfItems: {$sum: 1}}
	}
	])
db.fruits.aggregate([
		{
		$match: {"onsale": true}
	},
	{
		$group: {_id: "$supplier",  noOfItems: {$sum: 1}, totalstocks: {$sum: "$stocks"}}
	}
	])

//$out creates/overwrites a new collection a new collection from an existing collection
db.fruits.aggregate([
		{
		$match: {"onsale": true}
	},
	{
		$group: {_id: "$supplier", totalstocks: {$sum: "$stocks"}}
	},
	{
		$out: "totalStocksPerSupplier"
	}
	])



